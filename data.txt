# f x y = x * y - 5;
# h y = let y = i in i + 1; NICHT GÜLTIGER F CODE
# h y = let i = y + 5 in i + 1;
# main = h 2;
# h x y z = let i = x + z; j = y in i + j;
# part1 x z = let i = x + z in i + 9;
# part2 y = let j = y in j;
# h x y = let j = x; i = y in i + j;
# n = 5;
# test = 5 - 4;
# g b x y = if (b)
#      then x + y
#      else 1;
# g b x = if (b)
#      then 1 + 2
#      else x;
# j a = not a & true; geht nicht
# j a = a == true;
# j a = (a | false) == true;
# small a = 0 < a; #inconsistend with PushparamValue with Fin
# f a b = a + b;
# main = f 3 4;
main = let v1 = v2 ; v2 = 9 in f v1;
f x = g x;
g x = h x;
h x = j x;
j x = function x;
function x = x*3 + 4;
# main = let y = 5 in y + 1;
# main = 1 + 2 - 3 + 4;
# fac x = if x < 1
#        then 1
#        else x * fac ( x - 1);
# main = fac 4;
# main = f 1 true; f x y = g y x;

# main = f 1 2;
# g = 3;
