module TokenizerSpec (spec) where

import Test.Hspec

import PlatformIndependence
import Tokenizer

spec :: Spec
spec = do
    describe "Unit Test Tokenizer" $ do
        it "checks Tokenizer base Functionality" $ do
            fProgram <- platformIndependentReadFile "test/testinput.txt"
            tokenize fProgram `shouldBe` 
                ([Variablename "main",Keyword "=",Variablename "f",Number "5",Number "6",Punctuator
                ";",Variablename "f",Variablename "x",Variablename "y",Keyword "=",Variablename
                "x",Tokenizer.Operator "*",Variablename "y",Punctuator ";"] :: [Token])
