f x y = x * y - 5;
h x y z = let i = x + z; j = y in i + j;
n = 5;
test = 5 - 4;
g b x y = if (b)
      then x + y
      else 1;
j a = not a & true;
y a = (a | false) == true;
small a = 0 < a;
fac x = if x < 1
        then 1
        else x * fac ( x - 1);
# main = let v1 = v2 ; v2 = 9 in v1;
main = (f 1 2) + (h 1 2 3) + n;
