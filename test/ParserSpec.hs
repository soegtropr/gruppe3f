module ParserSpec (spec) where

import Test.Hspec

import PlatformIndependence
import Parser
import Tokenizer

spec :: Spec
spec = do
    describe "Unit Test Parser" $ do
        it "checks Parser base Functionality" $ do
            fProgram <- platformIndependentReadFile "test/testinput.txt"
            parseSynTree (tokenizeEither $ tokenize fProgram) `shouldBe`
                (Right (Programm [Definition "main" [] (Funkt (Funkt (Var "f") (Num 5)) (Num 6)),
                Definition "f" ["x","y"] (Binary (Var "x") (Times (Var "y")))]) :: Either String
                SynTree)

