module CompilerSpec (spec) where

import Test.Hspec

import PlatformIndependence
import Tokenizer
import Parser
import Compiler

spec :: Spec
spec = do
    describe "Unit Test Compiler" $ do
        it "checks Compiler base Functionality" $ do
            fProgram <- platformIndependentReadFile "test/testinput.txt"
            compilerHandler (parseSynTree (tokenizeEither $ tokenize fProgram)) `shouldBe`
                (Right (State (0, [Reset,Pushfun "main",Call,Halt,Pushparam
                1,Unwind,Call,Pushparam 3,Unwind,Call,Compiler.Operator (OpInt 2),Update
                Op,Return,Pushparam 1,Unwind,Call,Compiler.Operator Compiler.If,Update
                Op,Unwind,Call,Return,Pushparam 1,Unwind,Call,Compiler.Operator (OpInt 1),
                Update Op,Return,Pushparam 2,Pushparam 2,Pushpre OpTimes,Makeapp,Makeapp,
                Update (OpInt 2),Slide 3,Unwind,Call,Return,Pushval (Integer 6),
                Pushval (Integer 5),Pushfun "f",Makeapp,Makeapp,Update (OpInt 0),Slide 1,
                Unwind,Call,Return], ([],-1), [DEF "f" 2 27,DEF "main" 0 37], [("f",0),("main",1)])) :: Either String State)

        it "Regression Test" $ do
            fProgram <- platformIndependentReadFile "test/bigtest.txt"
            compilerHandler (parseSynTree (tokenizeEither $ tokenize fProgram)) `shouldBe` (Right (State (0,
                                                                                        [Reset,Pushfun "main",Call,Halt,Pushparam 1,Unwind,Call,Pushparam 3,Unwind,Call,Compiler.Operator (OpInt 2),Update Op,Return,Pushparam 1,Unwind,Call,Compiler.Operator Compiler.If,Update Op,Unwind,Call,Return,Pushparam 1,Unwind,Call,Compiler.Operator (OpInt 1),Update Op,Return,Alloc,Alloc,Makeapp,Alloc,Alloc,Makeapp,Pushparam (-1),UpdateLet 1,Pushval (Integer 9),UpdateLet 0,Pushparam 0,SlideLet 2,Update (OpInt 0),Slide 1,Unwind,Call,Return,Pushval (Integer 1),Pushparam 2,Pushpre OpSubBi,Makeapp,Makeapp,Pushfun "fac",Makeapp,Pushparam 2,Pushpre OpTimes,Makeapp,Makeapp,Pushval (Integer 1),Pushval (Integer 1),Pushparam 4,Pushpre OpLess,Makeapp,Makeapp,Pushpre OpIf,Makeapp,Makeapp,Makeapp,Update (OpInt 1),Slide 2,Unwind,Call,Return,Pushparam 1,Pushval (Integer 0),Pushpre OpLess,Makeapp,Makeapp,Update (OpInt 1),Slide 2,Unwind,Call,Return,Pushval (Compiler.Truthval True),Pushval (Compiler.Truthval False),Pushval (Compiler.Truthval True),Pushparam 4,Pushpre OpIf,Makeapp,Makeapp,Makeapp,Pushpre OpEq,Makeapp,Makeapp,Update (OpInt 1),Slide 2,Unwind,Call,Return,Pushval (Compiler.Truthval False),Pushval (Compiler.Truthval True),Pushparam 3,Pushpre OpNot,Makeapp,Pushpre OpIf,Makeapp,Makeapp,Makeapp,Update (OpInt 1),Slide 2,Unwind,Call,Return,Pushval (Integer 1),Pushparam 4,Pushparam 4,Pushpre OpAdd,Makeapp,Makeapp,Pushparam 3,Pushpre OpIf,Makeapp,Makeapp,Makeapp,Update (OpInt 3),Slide 4,Unwind,Call,Return,Pushval (Integer 4),Pushval (Integer 5),Pushpre OpSubBi,Makeapp,Makeapp,Update (OpInt 0),Slide 1,Unwind,Call,Return,Pushval (Integer 5),Update (OpInt 0),Slide 1,Unwind,Call,Return,Alloc,Alloc,Makeapp,Alloc,Alloc,Makeapp,Pushparam 5,Pushparam 4,Pushpre OpAdd,Makeapp,Makeapp,UpdateLet 1,Pushparam 4,UpdateLet 0,Pushparam (-1),Pushparam 1,Pushpre OpAdd,Makeapp,Makeapp,SlideLet 2,Update (OpInt 3),Slide 4,Unwind,Call,Return,Pushval (Integer 5),Pushparam 3,Pushparam 3,Pushpre OpTimes,Makeapp,Makeapp,Pushpre OpSubBi,Makeapp,Makeapp,Update (OpInt 2),Slide 3,Unwind,Call,Return],
                                                                                        ([],-1),
                                                                                        [DEF "main" 0 27,DEF "fac" 1 44,DEF "small" 1 70,DEF "y" 1 80,DEF "j" 1 96,DEF "g" 3 110,DEF "test" 0 126,DEF "n" 0 136,DEF "h" 3 142,DEF "f" 2 167],
                                                                                        [("main",0),("fac",1),("small",2),("y",3),("j",4),("g",5),("test",6),("n",7),("h",8),("f",9)])
                                                                            ) :: Either String State)