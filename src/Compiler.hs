module Compiler (
    State(State),
    PC,
    Code,
    Instruction(..),
    FunctionName,
    PossibleTypes(Truthval, Integer),
    CalcOp(..),
    OperatorKind(OpInt, If, Op),
    Arity,
    Top,
    Stack,
    StackElement(CODE, HEAP),
    HeapAddress,
    CodeAddress,
    Heap,
    HeapCell(..),
    Global,
    compilerHandler
) where

--Imports

import Parser

--Type Definitions

newtype State = State (PC, Code, Stack, Heap, Global) deriving(Eq) --Compiler Output

type PC             = Int

type Code           = [Instruction]
data Instruction    = Reset | Pushfun FunctionName | Pushval PossibleTypes | Pushparam Int | Makeapp
                    | Slide Int | Return | Halt | Unwind | Call | Pushpre CalcOp
                    | Update OperatorKind | Operator OperatorKind | Alloc | UpdateLet Int | SlideLet Int deriving (Eq)
type FunctionName   = String
data PossibleTypes  = Truthval Bool | Integer Int deriving(Show, Eq)
data CalcOp         = OpAdd | OpSubBi | OpSubUn | OpTimes | OpDiv | OpAnd | OpOr | OpEq | OpLess
                    | OpNot | OpIf deriving(Show, Eq)
data OperatorKind   = OpInt Arity | If | Op deriving(Show, Eq)
type Arity          = Int

type Top            = Int

type Stack = ([StackElement], Top)
data StackElement   = CODE CodeAddress | HEAP HeapAddress deriving(Show, Eq)
type HeapAddress        = Int
type CodeAddress        = Int

type Heap           = [HeapCell]
data HeapCell       = DEF FunctionName Arity CodeAddress | VAL PossibleTypes | APP HeapAddress HeapAddress
                    | IND HeapAddress | PRE CalcOp Arity | UNINITIALIZED deriving(Show, Eq)

type Global         = [(FunctionName, HeapAddress)]

--Type Instances

instance Show Instruction where
    show Reset         = "Reset"
    show (Pushfun a)   = "Pushfun " ++ show a
    show (Pushval a)   = "Pushval " ++ show a
    show (Pushparam a) = "Pushparam " ++ show a
    show Makeapp       = "Makeapp"
    show (Slide i)     = "Slide " ++ show i
    show Return        = "Return"
    show Halt          = "Halt"
    show Unwind        = "Unwind"
    show Call          = "Call"
    show (Pushpre a)   = "Pushpre " ++ show a
    show (Update a)    = "Update " ++ show a
    show (Operator a)  = "Operator " ++ show a
    show Alloc         = "Alloc"
    show (UpdateLet a) = "UpdateLet " ++ show a
    show (SlideLet a)  = "SlideLet " ++ show a

instance Show State where --See below for Function Definitions
    show (State(pc,code,stack,heap,global)) =
        "Compiler Output (Machine State): \nInstructions: \n" ++
        showInstructions pc code (length code) heap ++
        "Stack: " ++ show stack ++ "\n\n" ++
        "Heap: " ++ "\n" ++ "+-" ++ addbar (getHeapmaxfuncnamelength heap +
            getHeapmaxaritylength heap + getHeapmaxcodeaddrlength heap + 15) ++ "+\n" ++
            showHeap heap (getHeapmaxfuncnamelength heap) (getHeapmaxaritylength heap)
            (getHeapmaxcodeaddrlength heap) ++ "\n" ++
        "Global Environment: " ++ "\n" ++ "+-" ++ addbar(getGlobalmaxheapaddrlength
            global + getGlobalmaxfuncnamelength global + 5) ++ "+" ++ "\n" ++ showGlobal
            global (getGlobalmaxfuncnamelength global) (getGlobalmaxheapaddrlength global) ++
        "\n"

instance Semigroup State where
    (<>) = mappend
{-concatinates two States together.
    Output: State (0, Both instrucionlists concatinated, [],
            Both Heaps concatinated with corrected Instructionscounter,
            Both Golbal Environments concatinated with corrected Heapcounter)
-}
instance Monoid State where
    mempty = State (0,[],([],-1),[],[])
    mappend (State (_,inst1,_,heap1,global1))
            (State (_,inst2,_,[DEF functnameheap funcstell _],[(functnameglobal, _)])) =
                State (
                    0,
                    inst1++inst2,
                    ([],-1),
                    heap1 ++ [DEF functnameheap funcstell (length inst1)],
                    global1 ++ [(functnameglobal, length heap1)]
                )
    --For Predefined Functions
    mappend (State (_,inst1,_,heap1,global1))
        (State (_,inst2,_,[],[])) =
            State (
                0,
                inst1++inst2,
                ([],-1),
                heap1,
                global1
            )
    --For Linter uncoverd Patterns
    mappend (State (_,_,_,_,_)) (State (_,_,_,_,_)) = mempty

--Compiler

compilerHandler :: Either String SynTree -> Either String State
compilerHandler (Left errormessage) = Left errormessage
compilerHandler (Right syntree) = do
    let compiledState = compileProgram syntree
    checkMultipleFunctionDeclarations compiledState (globalofState compiledState) >>= checkExecutionErrors compiledState

compileProgram :: SynTree -> State
compileProgram (Programm []) = ((State (0, [Reset, Pushfun "main", Call, Halt], ([],-1), [], []) <>
    preDefinedBO) <> preDefinedIf) <> preDefinedUO
compileProgram (Programm (x:xs)) = mappend (compileProgram (Programm xs)) (compileDefinition x)

preDefinedIf :: State
preDefinedIf = State (0, [Pushparam 1, Unwind, Call, Operator Compiler.If, Update Op, Unwind, Call, Return],
                     ([],-1), [], [])

preDefinedBO :: State
preDefinedBO = State (0, [Pushparam 1,Unwind, Call, Pushparam 3, Unwind, Call, Operator (OpInt 2),
                         Update Op, Return], ([],-1), [], [])

preDefinedUO :: State
preDefinedUO = State (0, [Pushparam 1, Unwind, Call, Operator (OpInt 1), Update Op, Return],
                     ([],-1), [], [])

compileDefinition :: Def -> State
compileDefinition (Definition funName funParamList body) = State (
    0,
    fst (compileExpression (zip funParamList [1..]) body)++[Update (OpInt (length funParamList)),
    Slide (length funParamList + 1), Unwind, Call, Return],
    ([],-1),
    [DEF funName (length funParamList) 0],
    [(funName, 0)] )

compileExpression :: [(String, Int)] -> Expression -> (Code,[(String,Int)])
--For Every new Element (Num, Bool, Var) in Heap Add 1 to every Item in ParamList to offset HeapPosition
compileExpression paramList (Num numb) =
    ([Pushval (Integer numb)],map (\x -> (fst x, snd x + 1)) paramList)
compileExpression paramList (Truthvar boolean) =
    ([Pushval (Truthval boolean)], map (\x -> (fst x, snd x + 1)) paramList)
--ein If, ob der Parameter in der Parameterlist vorkommt.
compileExpression paramList (Var param) = if param `elem` map fst paramList
    then ([Pushparam (getPushParamValue paramList param)],map (\x -> (fst x, snd x + 1)) paramList)
    else ([Pushfun param],paramList)
--Subtract 2 to every Item in ParamList to offset Heap Position
compileExpression paramList (Parser.If boolean ifthen ifelse) = do
    let eval1 = compileExpression paramList ifelse
    let eval2 = compileExpression (snd eval1) ifthen
    let eval3 = compileExpression (snd eval2) boolean
    (fst eval1 ++ fst eval2 ++ fst eval3 ++ [Pushpre OpIf, Makeapp, Makeapp, Makeapp],
     map (\x -> (fst x, snd x - 2)) (snd eval3))
compileExpression paramList (Funkt (Var funcname) funcargs) = do
    let eval = compileExpression paramList funcargs
    --here no subtraction as it already got subtracted in the recursive Funktion call. 
    (fst eval ++ [Pushfun funcname] ++ [Makeapp],snd eval)
--For every Function call subtract 1 to every Item in ParamList to offset Heap Position
compileExpression paramList (Funkt funktrec funcargs) = do
    let eval1 = compileExpression paramList funcargs
    let eval2 = compileExpression (snd eval1) funktrec
    (fst eval1 ++ fst eval2 ++ [Makeapp],map (\x -> (fst x, snd x - 1)) (snd eval2))
compileExpression paramList (Binary a (Plus b)) = do
    let eval1 = compileExpression paramList b
    let eval2 = compileExpression (snd eval1) a
    (fst eval1 ++ fst eval2 ++ [Pushpre OpAdd,Makeapp, Makeapp],map (\x -> (fst x, snd x - 1)) (snd eval2))
compileExpression paramList (Binary a (Minus b)) = do
    let eval1 = compileExpression paramList b
    let eval2 = compileExpression (snd eval1) a
    (fst eval1 ++ fst eval2 ++ [Pushpre OpSubBi,Makeapp, Makeapp],map (\x -> (fst x, snd x - 1)) (snd eval2))
compileExpression paramList (Binary a (Times b)) = do
    let eval1 = compileExpression paramList b
    let eval2 = compileExpression (snd eval1) a
    (fst eval1 ++ fst eval2 ++ [Pushpre OpTimes,Makeapp, Makeapp],map (\x -> (fst x, snd x - 1)) (snd eval2))
compileExpression paramList (Binary a (Divide b)) = do
    let eval1 = compileExpression paramList b
    let eval2 = compileExpression (snd eval1) a
    (fst eval1 ++ fst eval2 ++ [Pushpre OpDiv,Makeapp, Makeapp],map (\x -> (fst x, snd x - 1)) (snd eval2))
compileExpression paramList (Binary a (Equals b)) = do
    let eval1 = compileExpression paramList b
    let eval2 = compileExpression (snd eval1) a
    (fst eval1 ++ fst eval2 ++ [Pushpre OpEq,Makeapp, Makeapp],map (\x -> (fst x, snd x - 1)) (snd eval2))
compileExpression paramList (Binary a (Smaller b)) = do
    let eval1 = compileExpression paramList b
    let eval2 = compileExpression (snd eval1) a
    (fst eval1 ++ fst eval2 ++ [Pushpre OpLess,Makeapp, Makeapp],map (\x -> (fst x, snd x - 1)) (snd eval2))
compileExpression paramList (Unary a) = compileExpression paramList a
compileExpression paramList (Neg a) = do
    let eval = compileExpression paramList a
    (fst eval ++ [Pushpre OpSubUn, Makeapp],snd eval)
compileExpression paramList (Not a) = do
    let eval = compileExpression paramList a
    (fst eval ++ [Pushpre OpNot, Makeapp],snd eval)
compileExpression paramList (Let localvar expr) = do
    let localparamList = getParamListforLocal paramList localvar
    let eval = compileExpression localparamList expr
    let localExpressioneval = getLocalExpressionInstructions localparamList localvar
    ([n | _ <- localvar, n <- [Alloc,Alloc,Makeapp]] ++ fst localExpressioneval ++ fst eval ++
        [SlideLet (length localvar)],localparamList)

compileExpression paramList _ = ([],paramList)

-- HELPER FUNCTIONS --

getPushParamValue :: [(String, Int)] -> String -> Int
getPushParamValue [] _ = 0
getPushParamValue (x:xs) param = if fst x == param then snd x else getPushParamValue xs param

getParamListforLocal :: [(String, Int)] -> [Localdefinition] -> [(String, Int)]
getParamListforLocal paramList [] = paramList
getParamListforLocal paramList ((Localdef localvarname _):xs) =
    getParamListforLocal (map (\x -> (fst x, snd x + 1)) paramList ++ [(localvarname, -1)]) xs

getLocalExpressionInstructions :: [(String, Int)] -> [Localdefinition] -> (Code,[(String,Int)])
getLocalExpressionInstructions paramList [] = ([],paramList)
getLocalExpressionInstructions paramList ((Localdef _ localexpr):xs) = do
    let eval = compileExpression paramList localexpr
    (fst eval ++ [UpdateLet (length xs)] ++ fst (getLocalExpressionInstructions paramList xs),
     snd (getLocalExpressionInstructions (snd eval) xs))

checkExecutionErrors :: State -> State -> Either String State
checkExecutionErrors state (State (pc, (Pushfun "main"):xs, stack, heap, global)) = case checkFunctionExists "main" heap of
                                                                                                Just 0 -> checkExecutionErrors state (State (pc , xs, stack, heap, global))
                                                                                                Just _ -> Left "Compiler Error: No Arguments for Main Function allowed."
                                                                                                Nothing -> Left "Compiler Error: No Main Function given."
checkExecutionErrors state (State (pc, (Pushfun funcname):xs, stack, heap, global)) = case checkFunctionExists funcname heap of
                                                                                                Just arity ->  if arity == length (takeWhile (== Makeapp) xs)
                                                                                                                    then checkExecutionErrors state (State (pc , xs, stack, heap, global))
                                                                                                                    else Left ("Compiler Error: Wrong number of Arguments given to " ++ funcname ++ findErrorLocation (length (codeofState state) - length xs) "" heap ++ ".")
                                                                                                Nothing -> Left ("Compiler Error: Found call for Function " ++ funcname ++ "," ++ findErrorLocation (length (codeofState state) - length xs) "" heap ++ ", which is not defined." )
checkExecutionErrors state (State (pc, _:xs, stack, heap, global)) = checkExecutionErrors state (State (pc , xs, stack, heap, global))
checkExecutionErrors state _ = Right state

checkMultipleFunctionDeclarations :: State -> Global -> Either String State
checkMultipleFunctionDeclarations state ((funcname,_):globals) = if funcname `elem` map fst globals
                                                                    then Left ("Compiler Error: Multiple Declarations for Function " ++ funcname ++ ".")
                                                                    else checkMultipleFunctionDeclarations state globals
checkMultipleFunctionDeclarations state [] = Right state

checkFunctionExists :: String -> Heap -> Maybe Int
checkFunctionExists funcname ((DEF name arity _):heaps) = if name == funcname
                                                                then Just arity
                                                                else checkFunctionExists funcname heaps
checkFunctionExists _ _ = Nothing

globalofState :: State -> Global
globalofState (State (_,_,_,_,global)) = global

codeofState :: State -> Code
codeofState (State (_,code,_,_,_)) = code

findErrorLocation :: Int -> String -> Heap -> String
findErrorLocation posinCode lastfuncname [DEF funcname _ pos] = if posinCode < pos
                                                   then " in the Definition of " ++ lastfuncname
                                                   else " in the Definition of " ++ funcname
findErrorLocation posinCode lastfuncname ((DEF funcname _ pos):heaps) = if posinCode < pos
                                                        then " in the Definition of " ++ lastfuncname
                                                        else findErrorLocation posinCode funcname heaps
findErrorLocation _ _ _ = ""

-- FOR SHOW ONLY --

showInstructions :: PC -> Code -> Int -> Heap -> String
showInstructions 4 (code:codes) codelength heap =
    "\n" ++ "<<< " ++ "binary-operator" ++ " >>>" ++ "\n\n" ++
    addspace (length(show codelength)-1) ++ "4" ++ ": " ++ show code ++ "\n" ++
    showInstructions (4+1) codes codelength heap
showInstructions 13 (code:codes) codelength heap =
    "\n" ++ "<<< " ++ "if" ++ " >>>" ++ "\n\n" ++
    addspace (length(show codelength)-2) ++ "13" ++ ": " ++ show code ++ "\n" ++
    showInstructions (13+1) codes codelength heap
showInstructions 21 (code:codes) codelength heap =
    "\n" ++ "<<< " ++ "unary-operator" ++ " >>>" ++ "\n\n" ++
    addspace (length(show codelength)-2) ++ "21" ++ ": " ++ show code ++ "\n" ++
    showInstructions (21+1) codes codelength heap
showInstructions pc (code:codes) codelength heap = do
    let eval1 = boolPCatFunction pc heap
    if fst eval1
        then "\n" ++ "<<< " ++ show(snd eval1) ++ " >>>" ++ "\n\n" ++
             addspace (length(show codelength)-length(show pc)) ++ show pc ++ ": " ++ show code ++ "\n" ++
             showInstructions (pc+1) codes codelength heap
        else addspace (length(show codelength)-length(show pc)) ++ show pc ++ ": " ++ show code ++ "\n" ++
             showInstructions (pc+1) codes codelength heap
showInstructions _ [] _ _ = "\n"

boolPCatFunction :: PC -> Heap -> (Bool,String)
boolPCatFunction pc (DEF funcn _ codeadd:heaps) = if pc == codeadd then (True,funcn)
                                                                   else boolPCatFunction pc heaps
boolPCatFunction _ _ = (False,[])

showHeap :: Heap -> Int -> Int -> Int -> String
showHeap ((DEF funcname arity codeaddr):heaps) maxfuncname maxarity maxcodaddr =
    "| DEF | " ++ addspace (maxfuncname - length funcname) ++ funcname ++ " | " ++
        addspace (maxarity - length (show arity)) ++ show arity ++ " | " ++
        addspace (maxcodaddr - length (show codeaddr)) ++ show codeaddr ++ " |" ++
        "\n+-" ++ addbar (maxfuncname + maxarity + maxcodaddr + 15) ++ "+\n" ++
        showHeap heaps maxfuncname maxarity maxcodaddr
showHeap [] _ _ _ = []
showHeap _ _ _ _ = []

addspace :: Int -> String
addspace 0 = " "
addspace a = if a > 0 then " " ++ addspace (a-1) else " " ++ addspace (a+1)

addbar :: Int -> String
addbar 0 = "-"
addbar a = "-" ++ addbar (a-1)

getHeapmaxfuncnamelength :: Heap -> Int
getHeapmaxfuncnamelength ((DEF funcname _ _):heaps) =
    max (length funcname) (getHeapmaxfuncnamelength heaps)
getHeapmaxfuncnamelength [] = 0
getHeapmaxfuncnamelength _ = 0

getHeapmaxaritylength :: Heap -> Int
getHeapmaxaritylength ((DEF _ arity _):heaps) =
    max (length (show arity)) (getHeapmaxaritylength heaps)
getHeapmaxaritylength [] = 0
getHeapmaxaritylength _ = 0

getHeapmaxcodeaddrlength :: Heap -> Int
getHeapmaxcodeaddrlength ((DEF _ _ codeaddr):heaps) =
    max (length (show codeaddr)) (getHeapmaxcodeaddrlength heaps)
getHeapmaxcodeaddrlength [] = 0
getHeapmaxcodeaddrlength _ = 0

showGlobal :: Global -> Int -> Int -> String
showGlobal ((funcname, heapaddr):globals) maxfuncname maxheapaddr =
    "| " ++ addspace (maxfuncname - length funcname) ++ funcname ++
    " | " ++ addspace (maxheapaddr - length (show heapaddr)) ++ show heapaddr ++
    " |" ++ "\n+-" ++ addbar (maxfuncname + maxheapaddr + 5) ++ "+\n" ++
    showGlobal globals maxfuncname maxheapaddr
showGlobal [] _ _ = []

{-
getGlobalmaxfuncnamelength :: Global -> Int
getGlobalmaxfuncnamelength (global:globals) =
    max (length (fst global)) (getGlobalmaxfuncnamelength globals)
getGlobalmaxfuncnamelength [] = 0;

getGlobalmaxheapaddrlength :: Global -> Int
getGlobalmaxheapaddrlength (global:globals) =
    max (length (show(snd global))) (getGlobalmaxheapaddrlength globals)
getGlobalmaxheapaddrlength [] = 0;
-}

getGlobalmaxfuncnamelength :: Global -> Int
getGlobalmaxfuncnamelength = foldr (max . length . fst) 0

getGlobalmaxheapaddrlength :: Global -> Int
getGlobalmaxheapaddrlength = foldr (max . length . show . snd) 0
