module InterpreterUI
    (
        interpreterUI,
        printEither,
        printAll,
        asciiArt
    ) where

import Control.Exception (SomeException, try)
import System.IO (stdout,hFlush) --needed for repl
import Text.Read
import PlatformIndependence
import Tokenizer
import Parser
import Compiler
import AbstractMachine

interpreterUI :: IO()
interpreterUI = do
    putStrLn "-----------------------------------------------------"
    putStrLn "                     FANG - REPL"
    putStrLn "               F-Language Interpreter"
    putStrLn "-----------------------------------------------------\n"
    putStrLn "Enter :h for Help."
    putStrLn "Enter your Program or Functions here:"
    recInput [] False

recInput :: [Token] -> Bool -> IO()
recInput currFProgram mlbool = do
    if mlbool then putStr "|> " else putStr "fang> "
    hFlush stdout
    currInput <- getLine
    case takeWhile (/= ' ') currInput of
        ":q" -> putStrLn "Quit."
        ":Q" -> putStrLn "Quit."
        ":quit" -> putStrLn "Quit."
        ":Quit" -> putStrLn "Quit."
        ":exit" -> putStrLn "Quit."
        ":Exit" -> putStrLn "Quit."
        ":info" -> do
                    putStrLn asciiArt
                    recInput currFProgram False
        ":reset" -> recInput [] False
        ":<<<" ->
                    recInput currFProgram True
        ":>>>" -> do
                    printEval currFProgram
                    recInput currFProgram False
        ":fe" -> do --Force Evaluation
                    printEval currFProgram
                    recInput currFProgram mlbool
        [] -> do
                    printEval currFProgram
                    recInput currFProgram mlbool
        ":load" -> do
                    newFProgram <- try (platformIndependentReadFile (drop 6 currInput)) :: IO (Either SomeException String)
                    case newFProgram of
                        Left exception -> do
                            print exception
                            recInput currFProgram False
                        Right program -> recInput (tokenize program) False
        ":delete" -> do
                    let tokennumber = snd (printCurrentEnvironment currFProgram 2)
                    let zipped = zip currFProgram tokennumber
                    let line = readEither (drop 8 currInput) :: Either String Int
                    case line of
                        Left exception -> do 
                            putStrLn (exception ++ " (expected Int)")
                            recInput currFProgram mlbool
                        Right number -> do
                            let cleared = [n | n <- zipped, snd n /= number+1]
                            recInput (map fst cleared) mlbool
        ":st" -> do --show current Tockens.
                    print currFProgram
                    recInput currFProgram mlbool
        ":sp" -> do --show Parser results.
                    printEither (parseSynTree $ tokenizeEither currFProgram)
                    recInput currFProgram mlbool
        ":sc" -> do --show Compiler results.
                    printEither (compilerHandler (parseSynTree $ tokenizeEither currFProgram))
                    recInput currFProgram mlbool
        ":smc"-> do --show Abstract Machine Evaluation Count
                    putStrLn $ "Machine Steps count: " ++ showMachineSteps (execute $ compilerHandler $ parseSynTree $ tokenizeEither currFProgram)
                    recInput currFProgram mlbool
        ":smt"-> do --show Abstract Machine Evaluation Steps
                    putStrLn . showHistory . snd $ execute (compilerHandler (parseSynTree $ tokenizeEither currFProgram))
                    recInput currFProgram mlbool
        ":se" -> do --show Current Environment (F-Program)
                    putStrLn "<<<"
                    putStr ("1: " ++ fst (printCurrentEnvironment currFProgram 2))
                    putStrLn ">>>"
                    recInput currFProgram mlbool
        ":h" -> do
                    putStrLn "Help for FANG Consol Interpreter:"
                    putStrLn "  To evaluate a Function, just enter the Name of the Function with Parameters and \";\" (no main declaration needed.). Every Function declaration has to end with \";\". Spreading function declarations over multiple lines is possible."
                    putStrLn "  :q            -> Quit"
                    putStrLn "  :info         -> Project Information"
                    putStrLn "  :reset        -> Reset Environment"
                    putStrLn "  :<<<          -> Beginning of Full F Program. (Has to contain a main function)"
                    putStrLn "  :>>>          -> End of Full F Program"
                    putStrLn "  :fe           -> Force Evaluation"
                    putStrLn "  <noInput>     -> Force Evaluation"
                    putStrLn "  :load <File>  -> Loads File into Environment. Warning: Resets current Environment."
                    putStrLn "  :delete <Int> -> Deletes Line out of Environment. (Line shown in :se)"
                    putStrLn "  :se           -> Shows current Environment as F-Program."
                    putStrLn "  :st           -> Shows current Environment tokenized."
                    putStrLn "  :sp           -> Shows current Environment parsed."
                    putStrLn "  :sc           -> Shows current Environment compiled."
                    putStrLn "  :smc          -> Shows the count of evaluationsteps of the current Environment."
                    putStrLn "  :smt          -> Shows the evaluationsteps of the current Environment."
                    putStr "\n"
                    recInput currFProgram mlbool
        _ -> if head currInput == ':'
                    then do
                        putStrLn "Unknown command. :h lists supported commands."
                        recInput currFProgram mlbool
                    else do
                        let newFProgram = createProgramOutOfInput currInput currFProgram
                        if snd newFProgram
                            then do
                                    let result = showResult (execute (compilerHandler (parseSynTree $ tokenizeEither (fst newFProgram))))
                                    if take 7 result == "Result:" then putStrLn (drop 8 result) else putStrLn result
                            else putStr ""
                        recInput (fst newFProgram) mlbool

createProgramOutOfInput :: String -> [Token] -> ([Token],Bool)
createProgramOutOfInput currInput currFProgram = do
    let tokenizedInput = tokenize currInput
    if checkInputtoMain tokenizedInput || head tokenizedInput == Keyword "let" || head tokenizedInput == Variablename "main" || head tokenizedInput == Keyword "if"
        then do
            let doubleRemovedFProgram = removeDoubleDefinition "main" (head currFProgram == Variablename "main") currFProgram
            if head tokenizedInput == Variablename "main"
                then (doubleRemovedFProgram ++ tokenizedInput,False)
                else (doubleRemovedFProgram ++ [Variablename "main",Keyword "="] ++ tokenizedInput,True)
        else if null currFProgram
            then (tokenizedInput, False)
            else case head tokenizedInput of
                    (Variablename a) -> do
                        let doubleRemovedFProgram = removeDoubleDefinition a (head currFProgram == Variablename a) currFProgram
                        (doubleRemovedFProgram ++ tokenizedInput,False)
                    _ -> (currFProgram ++ tokenizedInput,False)

checkInputtoMain :: [Token] -> Bool
checkInputtoMain ((Keyword "="):_) = False;
checkInputtoMain [Punctuator ";"] = True
checkInputtoMain [] = False
checkInputtoMain ((Keyword "let"):xs) = checkInputtoMain (tail (dropWhile (/= Keyword "in") xs))
checkInputtoMain ((Punctuator "("):xs) = checkInputtoMain (tail (dropWhile (/= Punctuator ")") xs))
checkInputtoMain ((Variablename _):xs) = checkInputtoMain xs
checkInputtoMain ((Tokenizer.Number _):xs) = checkInputtoMain xs
checkInputtoMain ((Tokenizer.Truthval _):xs) = checkInputtoMain xs
checkInputtoMain ((Tokenizer.Operator _):xs) = checkInputtoMain xs
--checkInputtoMain ((Keyword _):xs) = checkInputtoMain xs; Problems with if then else
checkInputtoMain ((Punctuator _):xs) = checkInputtoMain xs
checkInputtoMain _ = False

removeDoubleDefinition :: String -> Bool -> [Token] -> [Token]
removeDoubleDefinition _ _ [] = []
removeDoubleDefinition _ _ [Punctuator ";"] = [Punctuator ";"]
removeDoubleDefinition _ True (_:xs) = do
                                            let precheck = takeWhile (/= Punctuator ";") xs
                                            if Keyword "let" `elem` precheck
                                                then tail (dropWhile (/= Punctuator ";") (dropWhile (/= Keyword "in") xs))
                                                else tail (dropWhile (/= Punctuator ";") xs)
removeDoubleDefinition funcname False ((Punctuator ";"):xs) = if head xs == Variablename funcname
                                                            then do
                                                                let precheck = takeWhile (/= Punctuator ";") xs
                                                                if Keyword "let" `elem` precheck
                                                                    then Punctuator ";" : tail (dropWhile (/= Punctuator ";") (dropWhile (/= Keyword "in") xs))
                                                                    else Punctuator ";" : tail (dropWhile (/= Punctuator ";") xs)
                                                            else Punctuator ";" : removeDoubleDefinition funcname False xs
removeDoubleDefinition funcname False (x:xs) = x : removeDoubleDefinition funcname False xs

printCurrentEnvironment :: [Token] -> Int -> (String,[Int])
printCurrentEnvironment[Punctuator ";"] numb = (";\n",[numb])
printCurrentEnvironment((Punctuator ";"):xs) numb = (";\n" ++ show numb ++ ": " ++ fst (printCurrentEnvironment xs (numb+1)), numb:snd (printCurrentEnvironment xs (numb+1)))
printCurrentEnvironment((Keyword "let"):xs) numb = ("let " ++ fst(printCurrentEnvironmentLocal xs numb),numb:snd (printCurrentEnvironmentLocal xs numb))
printCurrentEnvironment(x:xs) numb = (printToken x ++ " " ++ fst(printCurrentEnvironment xs numb),numb:snd (printCurrentEnvironment xs numb))
printCurrentEnvironment [] 2 = ("No Environment to show.\n",[])
printCurrentEnvironment [] _ = ("",[])

printCurrentEnvironmentLocal :: [Token] -> Int -> (String,[Int])
printCurrentEnvironmentLocal((Keyword "in"):xs) numb = ("in " ++ fst (printCurrentEnvironment xs numb),numb:snd (printCurrentEnvironment xs numb))
printCurrentEnvironmentLocal(x:xs) numb =  (printToken x ++ " " ++ fst (printCurrentEnvironmentLocal xs numb), numb:snd (printCurrentEnvironmentLocal xs numb))
printCurrentEnvironmentLocal [] _ = ("",[])

printToken :: Token -> String
printToken (Punctuator x) = x
printToken (Variablename x) = x
printToken (Tokenizer.Truthval x) = x
printToken (Keyword x) = x
printToken (Tokenizer.Operator x) = x
printToken (Tokenizer.Number x) = x
printToken (ErrorMessage x) = x

printAll :: String -> IO()
printAll fProgram = do
                let tokenized = tokenizeEither $ tokenize fProgram
                let parsed = parseSynTree tokenized
                let compiled = compilerHandler parsed
                let executed = execute compiled
                putStrLn "Tokenized:"
                printEither tokenized
                printEither parsed
                printEither compiled
                putStrLn "Machine output:"
                putStrLn . showResult $ executed
                putStrLn ("Machine Steps count: " ++ showMachineSteps executed)
                putStrLn . showHistory . snd $ executed

printEval :: [Token] -> IO()
printEval program = do
    let result = showResult (execute (compilerHandler (parseSynTree $ tokenizeEither program)))
    if take 7 result == "Result:" then putStrLn (drop 8 result) else putStrLn result

printEither :: (Show a,Show b) => Either a b -> IO()
printEither (Right x) = print x
printEither (Left x) = print x

asciiArt :: String
asciiArt = "\
\                                                                                      \n\
\                            gggggg                                                        \n\ 
\                          NNNNNNNNNNNg                                                    \n\ 
\                         NNNNNNNNNNNNNNNg                                                 \n\ 
\                        gNNNNNNNNNNNNNNNNNNg                                              \n\ 
\                        NNNNNNNNNNNNNNNNNNNNNN                                            \n\ 
\                        NNNNNNNNNNNNNNNNNNNNNNNN                                          \n\ 
\                        NNNNNNNNNNNNNNNNNNNNNNNNNN                                        \n\ 
\                            HNNNNNNNNNNNNNNNNNNNNNNN                                      \n\ 
\                                 NNNNNNNNNNNNNNNNNNNNg                                    \n\ 
\                                    NNNNNNNNNNNNNNNNNNNk                                  \n\ 
\                                       NNNNNNNNNNNNNNNNNN                                 \n\ 
\                                         NNNNNNNNNNNNNNNNNk                               \n\ 
\                                           NNNNNNNNNNNNNNNNN                              \n\ 
\                                             NNNNNNNNNNNNNNNN                             \n\ 
\                                               NNNNNNNNNNNNNNNk                           \n\ 
\                                                 NNNNNNNNNNNNNNg                          \n\ 
\                                                  SNNNNNNNNNNNNNN                         \n\ 
\                                                  HNNNNNNNNNNNNNNN                        \n\ 
\                                                  HNNNNNNNNNNNNNNNN                       \n\ 
\                                                  gNNNNNNNNNNNNNNNNN                      \n\ 
\                                                  NNNNNNNNNNNNNNNNNNN            gggg     \n\ 
\ gfFfffffffffffN          KfN               fNg   NNNNNNNNNNNNNNNNNNN       gAf        N  \n\ 
\ g ggggggggggggN         g   g             H  Ng gNNNNNNNNNNNNNNNNNNNg    gf  ggNNNNNNggF \n\ 
\ g g                     F N N             H g N gNNNNNNNNNNNNNNNNNNNN   a  gNf           \n\ 
\ g g                    g gKg g            H gg NNNNNNNNNNNNNNNNNNNNNNk A gN              \n\ 
\ g g                    F N N N            H gHg NNNNNNNNNNNNNNNNNNNNNga gN               \n\ 
\ g g                   F N   N N           H gHgNg NNNNNNN  NNNNNNNNNNNkg                 \n\ 
\ g g                  g gF    g g          H gHNNNg NNNNNK  NNNNNNNNNNNgN                 \n\ 
\ g Eaaaaaaaaaag       F N     N N          H gNNNNN  NNNN   FNNNNNNNNNNNF                 \n\ 
\ g            N      g gF      k g         H gNNNNNN  NN    F NNNNNNNNNNF        aaaaaaag \n\ 
\ g gHHHHHHHHHHF      F N       N N         H gNNNNNNN AK    F NNNNNNNNNNk        K      gH\n\ 
\ g g          gHg   g   NNNNNNN   N        g gNNNNNNNN Ng   F NNNNNNNNNNK        fHHHHN gH\n\ 
\ g g          g N   F gNNNNNNNNNN Ng      gN gNNNNNNNKg Ng  F NNNNNNNNNNN             H gH\n\ 
\ g g          g N  g gF          k N     gNN gNNNNNNE  g N  k NNNNNNNNNNN             H gH\n\ 
\ g g          g N a gF            k N gNNNNN gNNNNK      g NL N NNNNNNNNN g           H gH\n\ 
\ g g          g N   N             N ANNNNNNN gNNN         g K N NNNNNNNNN  Ng         H gH\n\ 
\ g g          g Na gF              k NNNNNNN gNK          Gg  N  NNNNNNNNNg  Nag     ga gH\n\ 
\ g g          g N  N            ggNN gNNNNNN gH            Ng N  NNNNNNNN  Ngg         ggH\n\ 
\  NN          g NNN          ggNNNNNNNNNNNNNNN              fNF  NNNNNNNN    fNNNNNNNNNf  \n\ 
\             fN NggggggggggNNNNNNNNNNNNNNF                       HNNNNNNN                 \n\ 
\              g NNNNNNNNNNNNNNNNNNNNNNEgggggggggggggggggggggggggggNNNNNNgggggggggggggggggg\n\ 
\              g                                                   NNNNNN                 N\n\ 
\               NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNH\n\ 
\                                                                  SNNNK                   \n\ 
\                                                                   NN                     \n\ 
\                                                                                          \n\ 
\                                        FANG\n\
\                               F-Language Interpreter\n\
\\n\
\                   Developed during the LMU Course ImProg WS 21/22\n\
\\n\
\                                      Group 3F\n\
\\n\
\                                    Version 1.0\n\
\\n\
\            Repository: https://gitlab2.cip.ifi.lmu.de/soegtropr/gruppe3f\n\
\\n\
\"
