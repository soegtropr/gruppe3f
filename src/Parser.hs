{-# LANGUAGE BlockArguments #-}
module Parser
  ( SynTree (Programm),
    Def (Definition),
    Localdefinition (Localdef),
    Expression
      ( Funkt,
        Let,
        If,
        Unary,
        Binary,
        Plus,
        Minus,
        Not,
        Divide,
        Times,
        Equals,
        Neg,
        Smaller,
        Var,
        Num,
        Truthvar
      ),
    parseSynTree,

  )
where

import Data.Char
import Tokenizer

newtype SynTree
  = Programm [Def] deriving (Eq)

data Def
  = Definition String [String] Expression deriving (Show,Eq)

data Localdefinition
  = Localdef String Expression deriving (Show,Eq)

data Expression
  = Let [Localdefinition] Expression
  | If Expression Expression Expression
  | Funkt Expression Expression
  | Binary Expression Expression
  | Plus Expression
  | Minus Expression
  | Not Expression
  | Divide Expression
  | Times Expression
  | Equals Expression
  | Neg Expression
  | Smaller Expression
  | Var String
  | Num Int
  | Truthvar Bool
  | Unary Expression
  deriving (Show,Eq)


instance Show SynTree where
    show (Programm [])  = ""
    show (Programm x)  = let y = map show x in "\nSyntax tree:\n" ++ unlines y

type Parser a = Int -> [Token] -> Either String (Int,a, [Token])


parseSynTree :: Either String [Token] -> Either String SynTree
parseSynTree (Right [])  = Left(errorTrace 1 "Input" [])   
parseSynTree (Right ts1) = do (x2,defs,ts2) <- parseDefs 1 ts1
                              case ts2 of 
                                [] -> return (Programm defs)
                                _ -> Left(errorTrace x2 "; after Expression" ts2)
parseSynTree (Left errorMsg) = Left errorMsg

parseDefs :: Parser [Def]
parseDefs x1 ((Variablename s) : ts1) = do (x2, args, ts2) <- parseArgs (x1 + 1) ts1
                                           (x3, expr, ts3) <- parseExpr x2 ts2
                                           case ts3 of
                                             [Punctuator ";"] -> return (x3 + 1, [Definition s args expr], [])
                                             ((Punctuator ";") : ts4) ->  do (x4, defarr, ts5) <- parseDefs (x3 +1) ts4
                                                                             return (x4, Definition s args expr : defarr, ts5)
                                             _ ->  Left(errorTrace x3 "; after Expression" ts3)
parseDefs x1 ts1 = Left(errorTrace x1 "definition name" ts1)

parseArgs :: Parser [String]
parseArgs x1 ((Keyword "=") : ts1) = return (x1 + 1,[],ts1)
parseArgs x1 ((Variablename s) : ts1) = do (x2, args, ts2) <- parseArgs (x1 + 1) ts1
                                           return (x2, s : args ,ts2)
parseArgs x1 ts1 = Left(errorTrace x1 "= or function parameter" ts1)

parseExpr :: Parser Expression
parseExpr x1 (Keyword "let" : ts1) = parseLet (x1 + 1) ts1
parseExpr x1 (Keyword "if" : ts1) = do (x2,expr,ts2) <- parseExpr (x1 + 1) ts1
                                       case ts2 of
                                         (Keyword "then" : ts3) -> do (x3,expr2,ts4) <- parseExpr (x2 + 1) ts3;
                                                                      case ts4 of 
                                                                        (Keyword "else" : ts5) -> do (x4,expr3,ts6) <- parseExpr (x3 + 1) ts5
                                                                                                     return (x4, If expr expr2 expr3, ts6)
                                                                        _ -> Left(errorTrace x2 "Keyword else" ts4)
                                         _ -> Left(errorTrace x2 "Keyword then" ts2)
parseExpr x1 ts1 = parseOr x1 ts1

parseLet :: Parser Expression
parseLet x1 ts1 = do (x2,defs,ts2) <- parseLocDefs x1 ts1
                     (x3,expr,ts3) <- parseExpr x2 ts2
                     return (x3,Let defs expr,ts3)

parseLocDefs :: Parser [Localdefinition]
parseLocDefs x1 (Variablename s : Keyword "=" : ts1) = do (x2,expr,ts2) <- parseExpr (x1 + 2) ts1
                                                          case ts2 of
                                                            ((Keyword "in") : ts3) -> return (x2 + 1, [Localdef s expr], ts3)
                                                            ((Punctuator ";") : ts3) -> do (x3,defs,ts4) <- parseLocDefs (x2 + 1) ts3
                                                                                           return (x3, Localdef s expr : defs, ts4)
                                                            _ ->  Left( errorTrace  x2 "; or in after local definition" ts2 )
parseLocDefs x1 (Variablename s :ts1) = Left(errorTrace (x1 + 1) ("= after local funktion: " ++ s  ) ts1)
parseLocDefs x1 ts1 = Left(errorTrace x1 "local definition" ts1)
                                               
parseOr :: Parser Expression
parseOr x1 ts1 = do (x2,expr, ts2) <- parseAnd x1 ts1
                    case ts2 of
                      (Operator "|" : ts3) -> do (x3, expr2, ts4) <- parseAnd (x2 + 1) ts3
                                                 return (x3, If expr (Truthvar True) expr2, ts4) 
                      _ -> return (x2,expr, ts2)

parseAnd :: Parser Expression
parseAnd x1 ts1 = do (x2,expr, ts2) <- parseBinNot x1 ts1
                     case ts2 of
                       (Operator "&" : ts3) -> do (x3, expr2, ts4) <- parseAnd (x2 + 1) ts3
                                                  return (x3, If expr expr2 (Truthvar False), ts4) 
                       _ -> return (x2,expr, ts2)
                
parseBinNot :: Parser Expression
parseBinNot x1 (Operator "not" : ts1) =  do (x2,expr, ts2) <- parseCompOp (x1 + 1) ts1
                                            return (x2, Unary (Not expr), ts2)
parseBinNot x1 ts1 = parseCompOp x1 ts1

parseCompOp :: Parser Expression
parseCompOp x1 ts1 = do (x2,expr, ts2) <- parseAddSub x1 ts1
                        case ts2 of 
                          (Operator "<" : ts3) -> do (x3, expr2, ts4) <- parseAddSub (x2 + 1) ts3
                                                     return (x3, Binary expr (Smaller expr2), ts4)
                          (Operator "==" : ts3) -> do (x3, expr2, ts4) <- parseAddSub (x2 + 1) ts3
                                                      return (x3, Binary expr (Equals expr2), ts4)
                          _ -> return (x2,expr,ts2)

parseAddSub :: Parser Expression
parseAddSub x1 ts1 = do (x2,expr, ts2) <- parseBinNeg x1 ts1
                        case ts2 of
                             (Operator "-" : ts3) -> do (x3, expr2, ts4) <- parseBinNeg (x2 + 1) ts3
                                                        return (x3, Binary expr (Minus expr2), ts4)
                             _ -> do (x3, expr2, ts3) <- parseRestAdd x2 ts2
                                     return (x3, foldl Binary expr expr2, ts3)

parseRestAdd :: Parser [Expression]
parseRestAdd x1 (Operator "+" : ts1) = do (x2,expr, ts2) <- parseBinNeg (x1 + 1) ts1
                                          (x3,expr2, ts3) <- parseRestAdd x2 ts2
                                          return  (x3,Plus expr:expr2, ts3)
parseRestAdd x1 ts = return (x1,[], ts)

parseBinNeg :: Parser Expression
parseBinNeg x1 (Operator "-" : ts1) =  do (x2,expr, ts2) <- parseMultDiv (x1 + 1) ts1
                                          return (x2, Unary (Neg expr), ts2)
parseBinNeg x1 ts1 = parseMultDiv x1 ts1

parseMultDiv :: Parser Expression
parseMultDiv x1 ts1 = do (x2,expr, ts2) <- parseFun x1 ts1
                         case ts2 of
                           (Operator "/" : ts3) -> do (x3, expr2, ts4) <- parseFun (x2 + 1) ts3
                                                      return (x3, Binary expr (Divide expr2), ts4)
                           _ -> do (x3, expr2, ts3) <- parseRestMult x2 ts2
                                   return (x3, foldl Binary expr expr2, ts3)

parseRestMult :: Parser [Expression]
parseRestMult x1 (Operator "*" : ts1) = do (x2,e, ts2) <- parseFun (x1 + 1) ts1
                                           (x3,es, ts3) <- parseRestMult x2 ts2
                                           return  (x3,Times e:es, ts3)
parseRestMult x1 ts = return (x1,[], ts)

parseFun :: Parser Expression
parseFun x1 ts1 = do (x2,e, ts2) <- parseAtomic x1 ts1
                     (x3,es, ts3) <- parseRestFun x2 ts2
                     return  (x3, foldl Funkt e es, ts3)

parseRestFun :: Parser [Expression]
parseRestFun x1  ts1 
      | isFirstAtomic ts1 = do (x2,e, ts2) <- parseAtomic x1 ts1
                               (x3,es, ts3) <- parseRestFun x2 ts2
                               return  (x3,e:es, ts3)
      | otherwise = return (x1,[], ts1)

parseAtomic :: Parser Expression
parseAtomic x1 (Number i : ts) = return  (x1 + 1,Num (read i :: Int), ts)
parseAtomic x1 (Truthval b : ts) = return  (x1 + 1,Truthvar (read (toUpper (head b) : tail b) :: Bool), ts)
parseAtomic x1 (Variablename i : ts) = return  (x1 + 1,Var i, ts)
parseAtomic x1 (Punctuator "(" : ts1) = do (x2,e, ts2) <- parseExpr (x1 + 1 ) ts1 
                                           case ts2 of
                                             (Punctuator ")" : ts3) -> return (x2 + 1, e, ts3)
                                             x -> Left (errorTrace x2 "closing bracket" x)
parseAtomic x1 xs = Left (errorTrace x1 "expression" xs) 

errorTrace :: Int -> String -> [Token] -> String 
errorTrace i x [] = "Syntax Error at Token: " ++ show i  ++ " Expected " ++ x ++ " but found " ++ "end of file"
errorTrace i x (y:_) = "Syntax Error at Token: " ++ show i  ++ " Expected " ++ x ++ " but found "  ++ show y

isFirstAtomic :: [Token] -> Bool
isFirstAtomic (Number _ : _ ) = True
isFirstAtomic (Truthval _ : _) = True
isFirstAtomic (Variablename _ : _) = True
isFirstAtomic (Punctuator "(": _) = True
isFirstAtomic _ = False
