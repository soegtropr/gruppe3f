module Tokenizer (
    Token(Keyword,Number,Truthval,Variablename,Operator,Punctuator,ErrorMessage),
    tokenize,
    tokenizeEither
) where

import Data.Char
import Text.Printf (toChar)

data Token
    = Keyword String
    | Number String
    | Truthval String
    | Variablename String
    | Operator String
    | Punctuator String
    | ErrorMessage String deriving (Show, Eq)

tokenize :: String -> [Token]
tokenize "" = []
tokenize (x:code)
            | x == '#'                                       = tokenize $ dropWhile (/='\n') code -- Verwerfe alles ab dem '#'-Zeichen bis zum Zeilenbruch (=Kommentar)
            | x `elem` whitespace                            = tokenize $ dropWhile (`elem` whitespace) code
            | x `elem` punctuator                            = Punctuator [x]:tokenize code -- Da break mit Punctuators am Anfang die leere Liste zurückgibt, hier einfach über Pattern-Matching
            | x `elem` operatorChars                         = Operator [x]:tokenize code
            | maybeSpacedOp `elem` operatorStrings           = Operator maybeSpacedOp:tokenize opRest -- Damit der Operator (==) auch ohne eingrenzenden Whitespace funktioniert
            | spacedToken `elem` whitespaceSensitiveOps      = Operator spacedToken:tokenize spacedRest
            | x `elem` keywordChars                          = Keyword [x]:tokenize code
            | spacedToken `elem` whitespaceSensitiveKeywords = Keyword spacedToken:tokenize spacedRest
            | numOrVar `elem` bool                           = Truthval numOrVar:tokenize numVarRest
            | all isDigit numOrVar && numOrVar /= ""         = Number numOrVar:tokenize numVarRest
            | x `elem` allowedVarChars                       = Variablename numOrVar:tokenize numVarRest
            | otherwise                                      = [ErrorMessage $ "invalid input: \"" ++ [toChar x] ++ "\""]
        where
            (spacedToken, spacedRest) = break (`elem` (whitespace ++ punctuator)) (x:code) -- Schneidet den Code ab dem ersten Vorkommen von Whitespace oder Punctuator
            (maybeSpacedOp, opRest)   = span  (`elem` concat operatorStrings) (x:code) -- Schneide erst dann, wenn ein Zeichen vorkommt, die den Operator-Strings nicht gehört
            (numOrVar, numVarRest)    = span  (\c -> isDigit c || c `elem` allowedVarChars) (x:code) -- Schneide dann, wenn ein Zeichen vorkommt, das weder Zahl noch Buchstabe ist

tokenizeEither :: [Token] -> Either String [Token]
tokenizeEither [] = Right []
tokenizeEither tokens = case last tokens of
                            ErrorMessage msg -> Left msg
                            _                -> Right tokens


whitespace :: String
whitespace = " \n"

operatorChars :: String
operatorChars = "+-*/&|<"

operatorStrings :: [String]
operatorStrings = ["=="]

whitespaceSensitiveOps :: [String]
whitespaceSensitiveOps = ["not"]

keywordChars :: String
keywordChars = "="

whitespaceSensitiveKeywords :: [String]
whitespaceSensitiveKeywords = ["let", "in", "if", "then", "else"]

punctuator :: String
punctuator = "();"

bool :: [String]
bool = ["true", "false"]

allowedVarChars :: String
allowedVarChars = ['A'..'Z'] ++ ['a'..'z']
