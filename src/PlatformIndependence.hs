module PlatformIndependence where

import System.IO

platformIndependentReadFile:: FilePath -> IO String
platformIndependentReadFile path = do 
                                handle <- openFile path ReadMode
                                hSetNewlineMode handle universalNewlineMode
                                hGetContents handle